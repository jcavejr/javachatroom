package common;

/**
 * CommonDefines
 * 
 * List of common defines
 * @author jeff
 *
 */
public class CommonDefines {
	public final static int ILLEGAL_ARGUMENT_RC = 2;
	public final static String ADDRESS_OPTION_STRING = "-a";
	public final static String HELP_OPTION_STRING = "-h";
	public final static String PORT_OPTION_STRING = "-p";

	/**
	 * Private to prevent instantiation
	 */
	private CommonDefines() {

	}
}
