package common;

import java.io.Serializable;

public class MessageImpl implements MessageIF, Serializable {

	/**
	 * Eclipse generated serialVersionId for serialization
	 */
	private static final long serialVersionUID = -3466963247440448617L;
	private String messageContent;

	/**
	 * Create a MessageImpl
	 * @param messageContent content of the message
	 */
	public MessageImpl(String messageContent) {
		this.messageContent = messageContent;
	}
	
	/**
	 * Gets the message content
	 * @return message content
	 */
	public String getMessageContent() {
		return messageContent;
	}

}
