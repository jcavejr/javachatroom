package server;

import common.CommonDefines;

/**
 * Starts a chatroom server on specified port
 *
 */
public class App {

	public static void main(String[] args) {
		int portNumber = 0;
		try {
			portNumber = parseCommandLineArguments(args);
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
			printHelpMessage();
			System.exit(CommonDefines.ILLEGAL_ARGUMENT_RC);
		}
		System.out.println("Port: " + portNumber);
	}
	
	/**
	 * 
	 * @param args Array of command line arguments
	 * @return port number
	 */
	public static int parseCommandLineArguments(String[] args) {
		String portString = "";
		
		int i = 0;
		while (i < args.length) {
			String arg = args[i];
			if (arg.equals(CommonDefines.HELP_OPTION_STRING)) {
				printHelpMessage();
				System.exit(0);
			} else if (arg.equals(CommonDefines.PORT_OPTION_STRING)) {
				portString = args[i + 1];
				i += 2;
			} else {
				throw new IllegalArgumentException("Argument " + arg + " is invalid");
			}
		}
		
		int portNumber = Integer.parseInt(portString);
		return portNumber;
	}
	
	/**
	 * Displays a useful help message
	 */
	public static void printHelpMessage() {
		System.out.println("io.jeffcave.server v0.0.1\n");
		System.out.println("A server to deploy a chatroom.\n");
		System.out.println("Usage:");
		System.out.println("\tApp -p <port>\n");
		System.out.println("-h\t- Displays useful help message");
	}
}
