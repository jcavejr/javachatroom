package client;

import common.CommonDefines;

public class App {

	public static void main(String[] args) {
		RawConnectionInfo connectionInfo = null;
		try {
			connectionInfo = parseCommandLineArgs(args);
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
			System.out.println("\n");
			printHelpMessage();
			System.exit(CommonDefines.ILLEGAL_ARGUMENT_RC);
		}
		System.out.println(connectionInfo);

		// Start the sender and receiver threads
		SenderThread sender = new SenderThread();
		ReceiverThread receiver = new ReceiverThread();
		sender.start();
		receiver.start();

		// Set the main thread priority to 0
		Thread mainThread = Thread.currentThread();
		mainThread.setPriority(Thread.MIN_PRIORITY);
	}

	/**
	 * Parses command line arguments and attempts to create RawConnectionInfo
	 * 
	 * @param args command line args
	 * @return RawConnectionInfo
	 */
	static RawConnectionInfo parseCommandLineArgs(String[] args) {
		String ipAddressString = "";
		String portString = "";

		int i = 0;
		while (i < args.length) {
			String arg = args[i];
			if (arg.equals(CommonDefines.HELP_OPTION_STRING)) {
				printHelpMessage();
				System.exit(0);
			} else if (arg.equals(CommonDefines.ADDRESS_OPTION_STRING)) {
				ipAddressString = args[i + 1];
				i += 2;
			} else if (arg.equals(CommonDefines.PORT_OPTION_STRING)) {
				portString = args[i + 1];
				i += 2;
			} else {
				throw new IllegalArgumentException("Argument " + arg + " is invalid");
			}
		}

		return new RawConnectionInfo(ipAddressString, portString);
	}

	/**
	 * Print a help message to stdout
	 */
	static void printHelpMessage() {
		System.out.println("io.jeffcave.client v0.0.1\n");
		System.out.println("A client to connect to a chatroom server.\n");
		System.out.println("Usage:");
		System.out.println("\tApp -a <ip address> -p <port>\n");
		System.out.println("-h\t- Displays useful help message");
	}
}
