package client;

public class RawConnectionInfo {
	private String ipAddressString;
	private String portString;

	public RawConnectionInfo(String ipAddressString, String portString) {
		this.ipAddressString = ipAddressString;
		this.portString = portString;
	}

	public String getIpAddressString() {
		return ipAddressString;
	}

	public String getPortString() {
		return portString;
	}

	public String toString() {
		return "RawConnectionInfo: <ip=" + ipAddressString + "> <port=" + portString + ">";
	}
}
